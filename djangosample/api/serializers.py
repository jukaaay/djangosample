# -*- coding: utf-8 -*-

from blogs.models import Post
from django.contrib.auth.models import User
from rest_framework import serializers


class PostSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Post
