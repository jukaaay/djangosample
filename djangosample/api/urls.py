# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import PostList, PostDetail, UserPostList


urlpatterns = [
    url(r'^post$', PostList.as_view(), name='post-list'),
    url(r'^post/(?P<pk>[0-9]+)$', PostDetail.as_view(), name='post-list'),
    url(r'^post/(?P<username>[0-9a-zA-Z_-]+)$', UserPostList.as_view(), name='userpost-list')
]
